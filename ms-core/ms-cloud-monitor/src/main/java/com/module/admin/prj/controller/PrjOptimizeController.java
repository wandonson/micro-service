package com.module.admin.prj.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.module.admin.BaseController;
import com.module.admin.prj.pojo.PrjOptimize;
import com.module.admin.prj.service.PrjInfoService;
import com.module.admin.prj.service.PrjOptimizeService;
import com.system.comm.model.KvEntity;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 系统优化记录的Controller
 * @author yuejing
 * @date 2016-11-30 13:30:00
 * @version V1.0.0
 */
@Controller
public class PrjOptimizeController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrjOptimizeController.class);

	@Autowired
	private PrjOptimizeService prjOptimizeService;
	@Autowired
	private PrjInfoService prjInfoService;

	/**
	 * 跳转到管理页
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/prjOptimize/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		List<KvEntity> prjInfos = prjInfoService.findKvAll();
		modelMap.put("prjInfos", prjInfos);
		return "admin/prj/optimize-manager";
	}
	
	@RequestMapping(value = "/prjOptimize/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap,
			Integer id, Integer prjId) {
		PrjOptimize prjOptimize = null;
		if(id != null) {
			prjOptimize = prjOptimizeService.get(id);
		}
		if(prjOptimize == null) {
			prjOptimize = new PrjOptimize();
			prjOptimize.setPrjId(prjId);
		}
		modelMap.put("prjOptimize", prjOptimize);
		List<KvEntity> prjInfos = prjInfoService.findKvAll();
		modelMap.put("prjInfos", prjInfos);
		return "admin/prj/optimize-edit";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/prjOptimize/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			PrjOptimize prjOptimize) {
		ResponseFrame frame = null;
		try {
			frame = prjOptimizeService.pageQuery(prjOptimize);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	/**
	 * 保存
	 * @return
	 */
	@RequestMapping(value = "/prjOptimize/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response,
			PrjOptimize prjOptimize) {
		ResponseFrame frame = null;
		try {
			frame = prjOptimizeService.saveOrUpdate(prjOptimize);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/prjOptimize/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response,
			Integer id) {
		ResponseFrame frame = null;
		try {
			frame = prjOptimizeService.delete(id);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}
}