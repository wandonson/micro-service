package com.module.admin.prj.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.prj.dao.PrjAntDao;
import com.module.admin.prj.pojo.PrjAnt;
import com.module.admin.prj.service.PrjAntService;
import com.system.comm.model.Page;
import com.system.handle.model.ResponseFrame;
import com.system.handle.model.ResponseCode;

/**
 * prj_ant的Service
 * @author autoCode
 * @date 2018-01-30 20:32:37
 * @version V1.0.0
 */
@Component
public class PrjAntServiceImpl implements PrjAntService {

	@Autowired
	private PrjAntDao prjAntDao;
	
	@Override
	public ResponseFrame saveOrUpdate(PrjAnt prjAnt) {
		ResponseFrame frame = new ResponseFrame();
		if(prjAnt.getPaId() == null) {
			if(prjAnt.getPid() == null) {
				prjAnt.setPid(0);
			}
			prjAntDao.save(prjAnt);
		} else {
			prjAntDao.update(prjAnt);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public PrjAnt get(Integer paId) {
		return prjAntDao.get(paId);
	}

	@Override
	public ResponseFrame pageQuery(PrjAnt prjAnt) {
		prjAnt.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = prjAntDao.findPrjAntCount(prjAnt);
		List<PrjAnt> data = null;
		if(total > 0) {
			data = prjAntDao.findPrjAnt(prjAnt);
		}
		Page<PrjAnt> page = new Page<PrjAnt>(prjAnt.getPage(), prjAnt.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(Integer paId) {
		ResponseFrame frame = new ResponseFrame();
		prjAntDao.delete(paId);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
}