package com.module.admin.prj.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * prj_api_test实体
 * @author autoCode
 * @date 2018-03-08 10:59:40
 * @version V1.0.0
 */
@Alias("prjApiTest")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class PrjApiTest extends BaseEntity implements Serializable {
	//编号
	private Integer id;
	//项目编号
	private Integer prjId;
	//测试名称
	private String name;
	//创建时间
	private Date createTime;
	//测试时间
	private Date testTime;
	//测试结果
	private String testResult;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getPrjId() {
		return prjId;
	}
	public void setPrjId(Integer prjId) {
		this.prjId = prjId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getTestTime() {
		return testTime;
	}
	public void setTestTime(Date testTime) {
		this.testTime = testTime;
	}
	
	public String getTestResult() {
		return testResult;
	}
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}
}