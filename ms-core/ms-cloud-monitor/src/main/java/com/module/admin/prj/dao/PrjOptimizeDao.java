package com.module.admin.prj.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.module.admin.prj.pojo.PrjOptimize;

/**
 * prj_optimize的Dao
 * @author autoCode
 * @date 2018-05-31 15:51:51
 * @version V1.0.0
 */
public interface PrjOptimizeDao {

	public abstract void save(PrjOptimize prjOptimize);

	public abstract void update(PrjOptimize prjOptimize);

	public abstract void delete(@Param("id")Integer id);

	public abstract PrjOptimize get(@Param("id")Integer id);

	public abstract List<PrjOptimize> findPrjOptimize(PrjOptimize prjOptimize);
	
	public abstract int findPrjOptimizeCount(PrjOptimize prjOptimize);

	public abstract PrjOptimize getByPrjIdUrl(@Param("prjId")Integer prjId, @Param("url")String url);
}