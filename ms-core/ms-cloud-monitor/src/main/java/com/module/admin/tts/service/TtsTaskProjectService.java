package com.module.admin.tts.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.system.comm.model.KvEntity;

@Component
public interface TtsTaskProjectService {

	public Map<String, Object> get(Integer id);

	public List<KvEntity> findAll();

}
