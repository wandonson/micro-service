@echo off & setlocal enabledelayedexpansion

title ms-config

% 启动 %
echo Starting ...

java -Xms256m -Xmx256m -XX:MaxPermSize=64M -Dproject.dir=${user.dir}/../../ -jar ..\..\ms-cloud-config-2.0.1.jar

:end
pause