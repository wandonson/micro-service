package com.ms.env;


/**
 * 配置文件的key
 * @author yuejing
 * @date 2016年5月16日 下午5:52:03
 * @version V1.0.0
 */
public enum Env {
	HTTP_PORT		("http.port", "http的端口"),
	PROJECT_MODEL	("project.model", "项目模式[dev开发、test测试、release正式]"),
	/*CODE_TEMPLATE_PATH	("code.template.path", "模板存放路径"),
	CODE_SOURCE_PATH	("code.source.path", "源码存放路径"),*/

	PROJECT_TASK_THREAD_NUM	("project.task.thread.num", "任务的执行线程数,不设置默认为100"),
	
	PROJECT_DB_UPDATE	("project.db.update", "执行更新脚本[0否、1是，默认为1]"),
	;
	
	private String code;
	private String name;

	private Env(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
}