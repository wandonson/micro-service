package com.frame.test.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.test.service.TestService;
import com.monitor.rest.RestUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.system.comm.service.BaseService;
import com.system.comm.utils.FrameJsonUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

@Component
public class TestServiceImpl extends BaseService implements TestService {

	@Autowired
	private RestUtil restUtil;
	
	@Override
	@HystrixCommand(fallbackMethod = "errorServiceFallback")
	public String get(String id) {
		//通过restTemplate调用不通过才触发Hystrix
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		ResponseFrame res = restUtil.request("biz-service-api", "/test/get", params, "http://127.0.0.1:7900");
		return "get: " + res;
	}
	
	/**
	 * 异常服务的回调
	 * @return
	 */
	public String errorServiceFallback(String id) {
		ResponseFrame frame = new ResponseFrame();
		frame.setCode(ResponseCode.SERVER_ERROR_FALLBACK.getCode());
		frame.setMessage(ResponseCode.SERVER_ERROR_FALLBACK.getMessage());
        return FrameJsonUtil.toString(frame);
    }
}
