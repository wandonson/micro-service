@echo off & setlocal enabledelayedexpansion

title ms-eureka

% 启动 %
echo Starting ...

java -Xms256m -Xmx256m -XX:MaxPermSize=64M -Dproject.dir=${user.dir}/../../ -jar ..\..\ms-cloud-consul-client-2.0.1.jar

:end
pause